var graph = angular.module('graph',[]);

graph.controller('barGraphController', function ($scope) {
    $scope.graphData=[{
            name: 'Leads',
            data: [1, 3, 4, 7, 2,3,3]
        }, {
            name: 'Accepted',
            data: [2, 2, 3, 2, 1,2,3]
        }, {
            name: 'Follow-ups',
            data: [3, 4, 4, 2, 5,2,3]
        }];
    $scope.xaxis=['Apr 5', 'Apr 6', 'Apr 7', 'Apr 8', 'Apr 9','Apr 10','Apr 11'];
     Highcharts.setOptions({
            chart: {
                backgroundColor: 'white'
            },
            colors:["#5B677D","#BCEFF4","#F37F0B"],
            title: {
                style: {
                    color: 'silver'
                }
            },
            /*tooltip: {
                style: {
                    color: 'silver'
                }
            },*/
        exporting: {
         enabled: false
},
        credits: {
      enabled: false
  }
        });
    $('#barGraph').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: $scope.xaxis
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: false,
                /*style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }*/
            },
            labels: {
                enabled: false
            }
        },
        legend: {
            enabled:false,
        },
        tooltip: {
            enabled:false,
            headerFormat: '',
            pointFormat: ''
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
            }
        },
        series: $scope.graphData
    });
    
$scope.colors=["#F37F0B","#BCEFF4","#5B677D"];
$scope.totalLeads=0;
    $.each($scope.graphData[0].data,function() {
    $scope.totalLeads += parseInt(this);
});
$scope.totalAccepted=0;
    $.each($scope.graphData[1].data,function() {
    $scope.totalAccepted += parseInt(this);
});
$scope.totalFollowups=0;
    $.each($scope.graphData[2].data,function() {
    $scope.totalFollowups += parseInt(this);
});
});
graph.controller('circularGraphController', function ($scope) {
    $scope.colors =["#EAEAEA","#5B677D","#8BE1E8"];
     Highcharts.setOptions({
            chart: {
                backgroundColor: 'white'
            },
            colors:["#D1D0D0","#5B677D","#8BE1E8"],
            title: {
                style: {
                    color: 'silver'
                }
            },
            /*tooltip: {
                style: {
                    color: 'silver'
                }
            },*/
        exporting: {
         enabled: false
},
        credits: {
      enabled: false
  }
        });
    Highcharts.chart('circularGraph', {

        chart: {
            type: 'solidgauge',
            marginTop: 0,
        },
        
        title: {
            text: '',
            style: {
                fontSize: '34px'
            }
        },

        tooltip: { 
            borderWidth: 0,
			enabled: false,
            backgroundColor: 'none',
			visibility: 'show',
            shadow: false,
            style: {
                fontSize: '16px'
            },
            pointFormat: '{series.name}<br><span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}</span>',
            positioner: function (labelWidth, labelHeight) {
                return {
                    x: 232 - labelWidth / 2,
                    y: 75
                };
            }
        },

        pane: {
            startAngle: 0,
            endAngle: 360,
            background: [{ // Track for Move
                outerRadius: '106%',
                innerRadius: '95%',
                backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.3).get(),
                borderWidth: 0
            }, { // Track for Exercise
                outerRadius: '87%',
                innerRadius: '76%',
                backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.3).get(),
                borderWidth: 0
            }]
        },

        yAxis: {
            min: 0,
            max: 100,
            lineWidth: 0,
            tickPositions: []
        },

        plotOptions: {
            solidgauge: {
                borderWidth: '13px',
                dataLabels: {
                    enabled: false
                },
                linecap: 'square',
                stickyTracking: false
            }
        },

        series: [{
            name: 'Score',
            borderColor: Highcharts.getOptions().colors[1],
            data: [{
                color: Highcharts.getOptions().colors[1],
                radius: '100%',
                innerRadius: '100%',
                y: 85
            }]
        }, {
            name: 'Score',
            borderColor: Highcharts.getOptions().colors[2],
            data: [{
                color: Highcharts.getOptions().colors[2],
                radius: '80%',
                innerRadius: '80%',
                y: 75
            }]
        } 
        ]
    },

    function callback() {
    
    });
    
});

graph.controller('SplineController',function(){
      Highcharts.setOptions({
            chart: {
                backgroundColor: 'white',
                minPadding:0.5,
                maxPadding:0.5
            },
            title: {
                style: {
                    color: 'silver'
                }
            },
            exporting: {
                enabled: false
            },
            credits: {
                enabled: false
            }
        });
    $('#splineGraph').highcharts({
         title: {
            text: '',
            x: -20 //center
        },
        plotOptions: {
            series: {
                marker: {
                    enabled: true,
                    symbol: 'circle',
                    radius: 7,
                    lineWidth:2,
                lineColor: 'white',
                    
             }      
                }
            },
        colors:['#E3D9C3','#8BE1E8','#0E7F80'],
        
        xAxis: {
           visible:false
        },
        yAxis: {
            visible:false
        },
        tooltip: {
            enabled:false
        },
        legend: {
            enabled:false
        },
        series: [{
            name: 'Offer',
            data: [7 , 7 , 7 , 7 , 7, 7, 7,{ marker: {
                    fillColor: '#0E7F80',
                    lineWidth: 2,
                    lineColor: "white" 
            },y:7} , 7, 7, 7, 7, 7],
            lineWidth:5,
            zoneAxis: 'x',
            zones: [{
                value: 7,
                color: '#8BE1E8'
            }
            ,{
                color: '#E3D9C3'
            }]
        }]
    });
});

graph.controller("donutController", function($scope){
    $scope.donutData =[
                ['Buyers',   352],
                ['Sellers',       413],
                ['Buyers & Sellers', 230],
                ['Address Book',    535]
            ];
    $scope.colors = ["#0CCFCB","#0E8582","#82FD4F","#666666"];
    $scope.totalContacts = 0;
    $.each($scope.donutData, function(){
                  $scope.totalContacts += this[1];
        
    });
    console.log($scope.totalContacts);
     Highcharts.setOptions({
            chart: {
                backgroundColor: ''
            },
            colors:["#0CCFCB","#0E8582","#82FD4F","#EAEAEA"],
            title: {
                style: {
                    color: 'silver'
                }
            },
            
        exporting: {
         enabled: false
},
        credits: {
      enabled: false
  }
        });
   $('#donutGraph').highcharts({
    chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: $scope.totalContacts +'<br>Contacts<br>',
            align: 'center',
            verticalAlign: 'middle',
            y: 1,
            style: {
                        fontWeight: 'bolder',
                        color: 'black',
                        fontSize: '16px'
                    }
        },
        tooltip: {
            enabled:false
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: false,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white',
                        textShadow: '0px 1px 2px black'
                    }
                },
                startAngle: 0,
                endAngle: 360,
                center: ['50%', '50%']
            }
        },
        series: [{
            type: 'pie',
            name: 'Browser share',
            innerSize: '80%',
            data:$scope.donutData
        }]
   });
});