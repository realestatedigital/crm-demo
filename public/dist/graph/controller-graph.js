var graph = angular.module('graph',[]);

graph.controller("barGraphController", function ($scope) {
    
     Highcharts.setOptions({
            chart: {
                backgroundColor: 'white'
            },
            title: {
                enabled:false
            },
            
            exporting: {
                enabled: false
            },
            credits: {
                enabled: false
            }
        });
     $('#barGraph').highcharts({
        
        title: {
           enabled:false,
           text:''
        },
        subtitle: {
           enabled:false
        },
        xAxis: {
            type: 'category',
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            enabled:false,
            series: {
                borderWidth: 0,
                pointWidth: 50,
                dataLabels: {
                    enabled: false,
                }
                
            }
        },

        tooltip: {
            enabled:false
        },

        series: [{
            color: '#7C8697',
            maxPointWidth: 35,
            type:'column',
            data: [{
                name: 'Calls <br>made',
                y: 25
            }, {
                name: 'Emails <br>sent',
                y: 32.15
            }, {
                name: 'Text <br>sent',
                y: 79.38
            }, {
                name: 'Properties <br>viewed',
                y: 102.77
            }, {
                name: 'Properties <br>visited',
                y: 3.91
            }]
           
        }/*,{
             type:'line',
            inverted:true,
            data:[2, 2, 2, 2, 2]
        }*/]
       
         
    });
});